#include<iostream>

using namespace std;

class Engine
{
private:
	   int length;
    
public:
	   Engine(int l)
	   {
           length = l;
       }
    
	   int countStringsWithNoConsecutiveOnes()
	   {
           int endWith0 = 1;
           int endWith1 = 1;
           for(int i = 1 ; i < length ; i++)
           {
               int temp  = endWith0;
               endWith0 += endWith1;
               endWith1  = temp;
           }
           return (endWith0 + endWith1);
       }
};

int main()
{
    int length = 5;
    Engine e   = Engine(length);
    cout<<e.countStringsWithNoConsecutiveOnes()<<endl;
}
